var turno = 1; 
var i, j;
var matriz = [[],[],[]];

  function ganador(letra) //corrobora si existe un ganador en la partida tras cada jugada
  {
     if(      
		
	    (matriz[0][0] == matriz[0][1] && matriz[0][1] == matriz[0][2] && matriz[0][2] == letra) ||
	    (matriz[1][0] == matriz[1][1] && matriz[1][1] == matriz[1][2] && matriz[1][2] == letra) ||
            (matriz[2][0] == matriz[2][1] && matriz[2][1] == matriz[2][2] && matriz[2][2] == letra) ||
            (matriz[0][0] == matriz[1][0] && matriz[1][0] == matriz[2][0] && matriz[2][0] == letra) ||
            (matriz[0][1] == matriz[1][1] && matriz[1][1] == matriz[2][1] && matriz[2][1] == letra) ||
            (matriz[0][2] == matriz[1][2] && matriz[1][2] == matriz[2][2] && matriz[2][2] == letra) ||
            (matriz[0][2] == matriz[1][1] && matriz[1][1] == matriz[2][0] && matriz[2][0] == letra) ||
            (matriz[0][0] == matriz[1][1] && matriz[1][1] == matriz[2][2] && matriz[2][2] == letra) 
	)
	{
		alert("Jugador "+letra+ " GANA");
		window.location.reload();    //recarga la página 
        }
	else if (turno==9) {
		alert("Se han acabado los turnos. La partida termina en empate.");
		window.location.reload();  //recarga la página
	}
   }

  function cambiarValor(numero){
     var boton =  "#boton" + numero;	
     var jugador = turno%2;

      if ($(boton).val() == "")
      {		
	 if (numero>0 && numero<4){ i = 0; j = numero - 1; }
	 else if (numero>3 && numero<7){i = 1; j = numero - 4;}
         else {i = 2; j = numero - 7}
	
         if(jugador==1) //turno X impares
 	   {
		matriz[i][j] = ('X');
		$(boton).val("X");
                ganador("X"); 	
 	   }
	  else  //turno O pares
 	  {	
		matriz[i][j] = 'O';
		$(boton).val("O");
                ganador("O");
 	  }
          turno++;
      }
      else
      {
    	   alert("Esta celda ya se ha jugado!");
      }
   }
