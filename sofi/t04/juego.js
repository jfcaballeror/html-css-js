﻿//Arreglos:
//var miArreglo = new Array("hola",19,true);
//console.log(miArreglo, miArreglo[0]);
//clasName etiquetas css; tagName html


/*combinaciones ganadoras Hay 8 soluciones ganadoras
horizontales
012
345
678
verticales
036
147
258
cruzadas
048
246
*/

//DECLARAR OBJETOS Y VARIABLEs
var turno = 1;
var queTurno;
var arregloTateti =new Array(9);  //el arreglo empieza en cero

var celdas = document.getElementsByClassName("tateti");
// getElementsByClassName returns a collection of all elements in the document with the specified class name





//DECLARAR FUNCIONES
function gana(letra)  //función de combinaciones ganadoras
{
	//if (comb1 || comb2 || comb3 ......);
	if (
		(arregloTateti[0]==letra && arregloTateti[1]==letra && arregloTateti[2]==letra) ||
		(arregloTateti[3]==letra && arregloTateti[4]==letra && arregloTateti[5]==letra) || 
		(arregloTateti[6]==letra && arregloTateti[7]==letra && arregloTateti[8]==letra) ||
		(arregloTateti[0]==letra && arregloTateti[3]==letra && arregloTateti[6]==letra) ||
		(arregloTateti[1]==letra && arregloTateti[4]==letra && arregloTateti[7]==letra) ||
		(arregloTateti[2]==letra && arregloTateti[5]==letra && arregloTateti[8]==letra) ||
		(arregloTateti[0]==letra && arregloTateti[4]==letra && arregloTateti[8]==letra) ||
		(arregloTateti[2]==letra && arregloTateti[4]==letra && arregloTateti[6]==letra)
		)
	{
		alert("Jugador "+letra+ " GANA");
		window.location.reload(); //recarga la página
	}
}

function tateti(evento)
{
 	//alert("funciona tateti");
 	//alert(evento.target.id); //se imprime que id de celda clickeamos
 	var celda = evento.target; //celda que desencadena el evento
 	var idCelda = evento.target.id; // imprime el nombre del id
 	
 	//alert(idCelda.length); //numero de caracteres que contiene el nombre del id
	//alert (idCelda[1]); imprime el segundo caracter del idCelda q es el numero (idCelda[0]imprime la letra C
 	
	//numero q tiene como segunda posición idCelda menos 1 por el tema del array que empieza en cero
	var posicionAMarcar = idCelda[1]-1; //restamos para asignar esta variable al arregloTateti que empieza en cero
 	//console.log(posicionAMarcar);

 	queTurno = turno%2; //  residuo de turno dividido 2 //1/2=0,5

 	//if(queTurno!=0)
 	if(queTurno==1) //turno X impares
 	{
 		celda.innerHTML = "X"; //The innerHTML property sets or returns the HTML content (inner HTML) of an element.
 		celda.style.backgroundColor = "fuchsia";
 		arregloTateti[posicionAMarcar] = "X";
		celda.removeEventListener("click",tateti); //esto evita el doble click en una celda ya clickeada
 		gana("X"); //se llama a función gana para verificar si ya se cumplio una combinación ganadora
 	}

 	//turno O pares
 	//else if(queTurno == 0)
	else
 	{
 		celda.innerHTML = "O";
 		celda.style.backgroundColor = "purple";
 		arregloTateti[posicionAMarcar] = "O";
		celda.removeEventListener("click",tateti); //esto evita el doble click en una celda ya clickeada
 		gana("O");
 	}
 	console.log(turno,queTurno,arregloTateti); //imprime tambien el valor X O

 	if(turno == 9) // cantidad de turnos que puede existir solo 9
 	{
 		alert("Empate");
 		//console.log(window.location); //objeto location permite controlar toda la navegacion e historial del navegador
 		window.location.reload(); // se recarga la pagina luego de las 9 partidas

 	}
 	else
 	{
 		turno++; //cada vez que se ejecute la funcion tateti se debe aumentar turno
 		//console.log(turno,queTurno,posicionAMarcar); 
 	}
 	
 	

}


//esta es una función inicializadora, cada q cargamos una página sucede el windows load de la ventana
// se debe asignar el evento click a cada celda dibujada
function cargar()  
{	
	var n = 0; //inicia en cero por Array
		while(n<celdas.length) //la condicion <> true sale del while, miestras n sea menor al total de elementos de celdas=9
		{
		//console.log(n,celdas[n]);
		celdas[n].addEventListener("click",tateti); //se asigna el evento click a cada clase tateti.
		n++; //aumentamos el contador
		}
}

//ASIGNACION DE EVENTOS
window.addEventListener("load",cargar);
